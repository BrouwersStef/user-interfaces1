function makeAnchorditem(dic) {
    let a = document.createElement("a")
    a.href = dic.link
    a.className = "All " + dic.classes

    let article = document.createElement("article")
    a.appendChild(article)

    let h4 = document.createElement("h4")
    h4.className = "drop"
    h4.innerHTML = dic.name
    article.appendChild(h4)

    let img = document.createElement("img")
    img.className = "artikel"
    img.alt = dic.alt
    img.src = dic.srcc
    article.appendChild(img)

    let pName = document.createElement("p")
    pName.innerHTML = dic.name
    article.appendChild(pName)

    let pEuro = document.createElement("p")
    pEuro.innerHTML += "&euro; " + dic.price
    article.appendChild(pEuro)

    return a
}
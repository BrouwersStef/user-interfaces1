"use strict"
window.addEventListener("load", init)

function init() {
    addBestPic()
    addItemcard()
}

function addItemcard() {
    const name = new URLSearchParams(window.location.search).get("item")

    for (let categorie of cats) {
        for (let item of categorie.proddata) {
            if (item.name === name) {
                let anchor = document.getElementById("need")
                let element = makeAnchorditem(item)
                element.id = "product"
                if (item.link !== "") { //anders gaan items zonder proddetail fout
                    element.href = "../" + item.link
                }
                anchor.append(element)
            }
        }
    }
}

function addBestPic() {
    // if picture was uploaded to localstorage, display it
    if (localStorage.getItem("foto") !== null) {
        let pictu = document.createElement("img")
        pictu.id = "picture"
        pictu.alt = "image on the mug"
        pictu.src = localStorage.getItem("foto")
        pictu.height = 600
        let elm = document.getElementById("need")
        elm.append(pictu)
    }
}
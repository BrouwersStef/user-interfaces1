"use strict"
window.addEventListener("load", addData)

function addData() {
    for (let i in cats) {

        let section = document.createElement("section")
        section.className = cats[i].catName

        let h3 = document.createElement("h3")
        h3.id = cats[i].catName
        h3.className = "title"
        h3.innerHTML += cats[i].catNAME
        section.appendChild(h3)

        makeElms(cats[i].proddata, section)
        document.querySelector("section.producten").appendChild(section)
    }
}

function makeElms(arr, section) {
    for (let i = 0; i < arr.length; i++) {
        section.appendChild(makeAnchorditem(arr[i]))
    }
}
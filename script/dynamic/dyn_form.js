"use strict"
window.addEventListener("load", fillItem)

function fillItem() {
    let element = document.getElementById("item")
    for (let x of cats) {
        let optgroup = document.createElement("optgroup")
        optgroup.label = x.catName
        element.add(optgroup)

        for (let y of x.proddata){
            let option = document.createElement("option")
            option.value = y.name
            option.text = y.name
            optgroup.appendChild(option)
        }
    }
}
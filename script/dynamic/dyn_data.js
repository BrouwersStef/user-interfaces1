"use strict"

const dyn_cilinderTas_data =
    [
        {
            name: "The Mug The myth the legend",
            classes: "White",
            link: "ProductDetail/cilinderTas/productDetailPage-The_Mug_the_myth_the_legend.html",
            alt: "image of The Mug The myth the legend. right handed mug. front side of mug",
            srcc: "/media/smallImages/cilindertassen/The%20Mug%20The%20myth%20the%20legend.png",
            price: "15.00"
        },
        {
            name: "Caffeine Commander",
            classes: "Blue",
            link: "ProductDetail/cilinderTas/productDetailPage-Caffeine_Commander.html",
            alt: "image of Caffeine Commander. right handed mug. front side of mug",
            srcc: "/media/smallImages/cilindertassen/Caffeine%20Commander.png",
            price: "16.50"
        },
        {
            name: "Espresso Yourself",
            classes: "Black",
            link: "ProductDetail/cilinderTas/productDetailPage-Espresso_Yourself.html",
            alt: "image of Espresso Yourself. right handed mug. front side of mug",
            srcc: "/media/smallImages/cilindertassen/Espresso%20Yourself.png",
            price: "12.75"
        },
        {
            name: "Coffeeholic",
            classes: "White",
            link: "",
            alt: "image of Coffeeholic. right handed mug. front side of mug",
            srcc: "/media/smallImages/cilindertassen/Coffeeholic.png",
            price: "24.20"
        },
        {
            name: "Brewtiful",
            classes: "Blue",
            link: "",
            alt: "image of Brewtiful. left handed mug. front side of mug",
            srcc: "/media/smallImages/cilindertassen/Brewtiful.png",
            price: "11.80"
        },
        {
            name: "Mugsy",
            classes: "Blue",
            link: "",
            alt: "image of Mugsy. right handed mug. front side of mug",
            srcc: "/media/smallImages/cilindertassen/Mugsy.png",
            price: "19.30"
        },
        {
            name: "Morning Boost",
            classes: "Black",
            link: "",
            alt: "image of Morning Boost. right handed mug. front side of mug",
            srcc: "/media/smallImages/cilindertassen/Morning%20Boost.png",
            price: "13.90"
        },
        {
            name: "Brew-tiful Morning",
            classes: "Green",
            link: "",
            alt: "image of Brew-tiful Morning. right handed mug. front side of mug",
            srcc: "/media/smallImages/cilindertassen/Brew-tiful%20Morning.png",
            price: "22.15"
        },
        {
            name: "Brewed Awakening",
            classes: "Green",
            link: "",
            alt: "image of Brewed Awakening. right handed mug. front side of mug",
            srcc: "/media/smallImages/cilindertassen/Brewed%20Awakening.png",
            price: "10.40"
        },
    ]

const dyn_curvedKoffieTas_data =
    [
        {
            name: "Bean There, Done That",
            classes: "Blue",
            link: "",
            alt: "image of Bean There, Done That. right handed mug. front side of mug",
            srcc: "/media/smallImages/curvedKoffietas/Bean%20There,%20Done%20That.png",
            price: "15.00"
        },
        {
            name: "Coffee A-Holic",
            classes: "Green",
            link: "",
            alt: "image of Coffee A-Holic. left handed mug. front side of mug",
            srcc: "/media/smallImages/curvedKoffietas/Coffee%20A-Holic.png",
            price: "23.70"
        },
        {
            name: "Mugshot",
            classes: "White",
            link: "",
            alt: "image of Mugshot. right handed mug. front side of mug",
            srcc: "/media/smallImages/curvedKoffietas/Mugshot.png",
            price: "14.25"
        },
        {
            name: "Savory Sips",
            classes: "White",
            link: "",
            alt: "image of Savory Sips. right handed mug. front side of mug",
            srcc: "/media/smallImages/curvedKoffietas/Savory%20Sips.png",
            price: "21.05"
        },
        {
            name: "Cinnamon Bliss",
            classes: "Black",
            link: "",
            alt: "image of Cinnamon Bliss. left handed mug. front side of mug",
            srcc: "/media/smallImages/curvedKoffietas/Cinnamon%20Bliss.png",
            price: "17.60"
        }]

const dyn_tasZonderOor_data =
    [
        {
            name: "Mugnificent Bastard",
            classes: "Blue",
            link: "",
            alt: "image of Mugnificent Bastard. multi-handed mug. front side of mug",
            srcc: "/media/smallImages/tasZonderoor/Mugnificent%20Bastard.png",
            price: "15.00"
        },
        {
            name: "bukkit full of HOT SAUCE",
            classes: "Black",
            link: "",
            alt: "image of bukkit full of HOT SAUCE. multi-handed mug. front side of mug",
            srcc: "/media/smallImages/tasZonderoor/bukkit%20full%20of%20HOT%20SAUCE.png",
            price: "10.75"
        },
        {
            name: "The Caffeine Connoisseur",
            classes: "Blue",
            link: "",
            alt: "image of The Caffeine Connoisseur. multi-handed mug. front side of mug",
            srcc: "/media/smallImages/tasZonderoor/The%20Caffeine%20Connoisseur.png",
            price: "12.20"
        },
        {
            name: "The Latte Lover",
            classes: "Black",
            link: "",
            alt: "image of The Latte Lover. multi-handed mug. front side of mug",
            srcc: "/media/smallImages/tasZonderoor/The%20Latte%20Lover.png",
            price: "21.05"
        },
        {
            name: "The Cup of Joe King",
            classes: "Green",
            link: "",
            alt: "image of The Cup of Joe King. multi-handed mug. front side of mug",
            srcc: "/media/smallImages/tasZonderoor/The%20Cup%20of%20Joe%20King.png",
            price: "10.75"
        },
        {
            name: "The Roast Renegade",
            classes: "White",
            link: "",
            alt: "image of The Roast Renegade. multi-handed mug. front side of mug",
            srcc: "/media/smallImages/tasZonderoor/The%20Roast%20Renegade.png",
            price: "15.80"
        },
    ]

const cats =
    [
        {
            catName: "cilinderTas",
            catNAME: "CilinderTassen",
            proddata: dyn_cilinderTas_data
        },
        {
            catName: "curvedKoffieTas",
            catNAME: "curvedKoffietassen",
            proddata: dyn_curvedKoffieTas_data
        },
        {
            catName: "tasZonderOor",
            catNAME: "tasssenZonderoor",
            proddata: dyn_tasZonderOor_data
        }
    ]
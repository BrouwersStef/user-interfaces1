"use strict"
window.addEventListener("load", addData)

function addData() {
    const url = new URLSearchParams(window.location.search)

    //fill all the cells
    const elementen = document.querySelectorAll("tr td:last-child")
    for (const element of elementen) {
        element.innerHTML = url.get(element.id)
    }

    //give color and replace whit Nee
    for (const x of elementen) {
        let text = x.innerHTML
        //if color set background
        if (text.startsWith("#")) {
            x.style.background = text
        }
        // if undefined or "" or null --> Nee
        if (text === "undefined" || text === "" || text === null) {
            x.innerHTML = "Nee"
        }
    }
}
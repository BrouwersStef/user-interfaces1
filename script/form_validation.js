"use strict"
window.addEventListener("load", addHandlers)

function addHandlers() {
    document.getElementById("naam").addEventListener("focusout", naamValidatie)
    document.getElementById("email").addEventListener("input", emailValidaie)
    document.getElementById("phone").addEventListener("input", phoneValidate)
    document.getElementById("form").addEventListener("submit", formEindValidatie)

    //clear pic
    localStorage.removeItem("foto")
    //picture to localstorage
    let element = document.getElementById("foto")
    element.addEventListener("change", () => {
        const fr = new FileReader()
        fr.readAsDataURL(element.files[0])

        //file reader is async
        fr.addEventListener("load", () => {
            localStorage.setItem("foto", fr.result)
        })
    })
}

function naamValidatie() { //done
    let element = document.getElementById("naam")
    let span = document.getElementById("naamValidation")

    // HERE naam validatie

    if (element.value.startsWith(" ") || element.value.endsWith(" ")) {
        span.innerHTML = "Er staan spaties in het begin of einde van de naam"
        element.classList.remove("validinput")
        return false
    } else if (element.value === "") {
        span.innerHTML = "naam is empty"
        element.classList.remove("validinput")
        return false
    } else {
        span.innerHTML = ""
        element.classList.add("validinput")
        return true
    }
}

function emailValidaie() {
    let element = document.getElementById("email")
    let span = document.getElementById("emailValidation")
    let reg = /^(\w+\.)+\w+@(student\.)?kdg\.be$/gi //to match email

    // HERE email validatie
    if (reg.test(element.value)) {
        span.innerHTML = ""
        element.classList.add("validinput")
        return true
    } else if (element.value === "") {
        span.innerHTML = "email is empty"
        element.classList.remove("validinput")
        return false
    } else {
        span.innerHTML = "geen valide email"
        element.classList.remove("validinput")
        return false
    }
}

function phoneValidate() {
    let element = document.getElementById("phone")
    let span = document.getElementById("phoneValidation")
    let reg = /^(((\+|00)32[ ]?(?:\(0\)[ ]?)?)|0){1}(4(60|[789]\d)\/?(\s?\d{2}\.?){2}(\s?\d{2})|(\d\/?\s?\d{3}|\d{2}\/?\s?\d{2})(\.?\s?\d{2}){2})$/gi

    // span.innerHTML = element.value
    // HERE phone validatie

    if (reg.test(element.value)) {
        span.innerHTML = ""
        element.classList.add("validinput")
        return true
    } else if (element.value === "") {
        span.innerHTML = "gsm number is empty"
        element.classList.remove("validinput")
        return false
    } else {
        span.innerHTML = "geen valide belgish gsm number"
        element.classList.remove("validinput")
        return false
    }
}

function formEindValidatie(event) {
    // let element = document.getElementById("submit")
    let span = document.getElementById("submitValidatie")

    // HERE end form validatie
    if (naamValidatie() && emailValidaie() && phoneValidate()) {
        span.innerHTML = ""
    } else {
        span.innerHTML = "Er zijn nog problemen op het form"
        event.preventDefault()
    }
}
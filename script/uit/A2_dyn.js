"use strict"
window.addEventListener("load", init)

function init() {
    setminmax()
    document.getElementById("price-max").addEventListener("input", displaycurval)
    document.getElementById("price-min").addEventListener("input", displaycurval)

    document.getElementById("price-max").addEventListener("input", hideitems)
    document.getElementById("price-min").addEventListener("input", hideitems)
    //     *********************************
    document.getElementById("prodname").addEventListener("input", hideitemsName)

}

function setminmax() {
    let slidermax = document.getElementById("price-max")
    let slidermin = document.getElementById("price-min")

    let x;
    let y;
    let prices = []

    for (x of cats) {
        for (y of x.proddata) {
            prices.push(y.price)
        }
    }

    let maxprices = Math.ceil(prices.sort().reverse()[0])
    let minprices = Math.floor(prices.sort()[0])

    slidermax.max = maxprices
    slidermax.min = minprices
    slidermax.value = maxprices

    slidermin.max = maxprices
    slidermin.min = minprices
    slidermin.value = minprices

    displaycurval()
}

function displaycurval() {
    let slidermax = document.getElementById("price-max")
    let spanmax = document.getElementById("currpricevalmax")
    let slidermin = document.getElementById("price-min")
    let spanmin = document.getElementById("currpricevalmin")

    spanmax.innerHTML = slidermax.value
    spanmin.innerHTML = slidermin.value
}

function hideitems() {
    let max = document.getElementById("price-max").value
    let min = document.getElementById("price-min").value
    let artikels = document.getElementsByClassName("All")
    let a;

    for (a of artikels) {
        let price = a.firstChild.lastChild.textContent.substring(2)

        if (price > min && price < max) {
            a.classList.remove("hidden")
        } else {
            a.classList.add("hidden")
        }
    }
}

function hideitemsName() {
    let text = document.getElementById("prodname").value
    let artikels = document.getElementsByClassName("All")
    let a;

    for (a of artikels) {
        let naam = a.firstChild.firstChild.textContent
        if (naam.toLowerCase().includes(text.toLowerCase())) {
            a.classList.remove("hidden")
        } else {
            a.classList.add("hidden")
        }
    }
}